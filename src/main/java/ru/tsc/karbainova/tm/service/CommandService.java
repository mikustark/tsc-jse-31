package ru.tsc.karbainova.tm.service;

import lombok.NonNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.api.repository.ICommandRepository;
import ru.tsc.karbainova.tm.api.service.ICommandService;
import ru.tsc.karbainova.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.Map;

public class CommandService implements ICommandService {
    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public AbstractCommand getCommandByName(@NonNull final String name) {
        if (name.isEmpty()) return null;
        return commandRepository.getCommandByName(name);
    }

    @Override
    public AbstractCommand getCommandByArg(@Nullable final String arg) {
        if (arg == null || arg.isEmpty()) return null;
        return commandRepository.getCommandByArg(arg);
    }

    @Override
    public Map<String, AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

    @Override
    public Map<String, AbstractCommand> getArguments() {
        return commandRepository.getArguments();
    }

    @Override
    public Collection<String> getListCommandName() {
//        return null;
        return commandRepository.getCommandName();
    }

    @Override
    public Collection<String> getListCommandArg() {
        return commandRepository.getCommandArg();
    }

    @Override
    public void create(AbstractCommand command) {
        if (command == null) return;
        commandRepository.create(command);
    }


}
