package ru.tsc.karbainova.tm.command.task;

import ru.tsc.karbainova.tm.command.ProjectAbstractCommand;
import ru.tsc.karbainova.tm.command.TaskAbstractCommand;
import ru.tsc.karbainova.tm.enumerated.Role;
import ru.tsc.karbainova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.karbainova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.model.Task;
import ru.tsc.karbainova.tm.util.TerminalUtil;

public class TaskShowByIdCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "find-by-id-task";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Find task by id";
    }

    @Override
    public void execute() {
        String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        show(task);
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }
}
