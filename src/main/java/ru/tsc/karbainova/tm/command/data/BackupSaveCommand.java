package ru.tsc.karbainova.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.dto.Domain;
import ru.tsc.karbainova.tm.enumerated.Role;

import java.io.FileOutputStream;

public class BackupSaveCommand extends AbstractDataCommand {

    public static String BACKUP_SAVE = "backup-save";

    @Override
    public String name() {
        return BACKUP_SAVE;
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Save Backup";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NonNull final Domain domain = getDomain();
        @NonNull final ObjectMapper objectMapper = new XmlMapper();
        @NonNull final String xml = objectMapper.writeValueAsString(domain);
        @NonNull final FileOutputStream fileOutputStream = new FileOutputStream(BACKUP_XML);
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    public @Nullable Role[] roles() {
        return new Role[0];
    }
}
